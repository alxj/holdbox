package org.alxj.holdbox;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

public class HoldboxInstance extends Thread {
    // "isInitialized = true" allows thread to be started.
    private static boolean isInitialized = false;
    // "isRunning = true" guarantees thread to be running.
    private static boolean isRunning = false;

    private File sourceFolder;
    private int[] counters = new int[2];
    private static int SCANNED_FILES = 0, NEW_FILES = 1;
    private static long startTime;

    private List<String> sourceFolderPaths = new ArrayList<String>();
    private List<String> heldFolderPaths = new ArrayList<String>();
    private int scanningIntervalMillis = 5000;

    public HoldboxInstance(String settingsPath) {
        isInitialized = false;
        if (loadSettings(settingsPath) < 0)
            return;
        if (checkSettings() < 0)
            return;
        isInitialized = true;
    }

    public static boolean isInitialized() {
        return isInitialized;
    }

    public static boolean isRunning() {
        return isRunning;
    }

    public static long getStartTime() {
        return startTime;
    }

    public void run() {
        if (!isInitialized) {
            finish();
            return;
        }
        startTime = System.currentTimeMillis();
        isRunning = true;
        Log.info("Holdbox Instance is running. It will process " + sourceFolderPaths.size() + " folders:");
        for (int i = 0; i < sourceFolderPaths.size(); ++i) {
            Log.info("    from " + sourceFolderPaths.get(i) + " to " + heldFolderPaths.get(i));
        }
        for (;;) {
            counters[SCANNED_FILES] = 0;
            counters[NEW_FILES] = 0;
            Log.info("Source folders scanning started at "
                    + SystemFunctionality.getDateAndTime(System.currentTimeMillis()) + ".");
            for (int i = 0; i < sourceFolderPaths.size(); ++i) {
                scanFolderRecursively(sourceFolderPaths.get(i), heldFolderPaths.get(i),
                        new File(sourceFolderPaths.get(i)), counters, 0);
            }
            Log.info("Source folders scanning finished (total files: " + counters[SCANNED_FILES] + ", new files: "
                    + counters[NEW_FILES] +", total errors: " + Log.ERRORS + ").");
            try {
                Thread.sleep(scanningIntervalMillis);
            } catch (InterruptedException e) {
                finish();
                break;
            }
        }
    }

    private void finish() {
        isRunning = false;
        Thread.currentThread().interrupt();
        Log.info("Holdbox Instance is stopped.");
    }

    private int loadSettings(String path) {
        InputStream inputStream;
        BufferedReader bufferedReader;
        String line;
        try {
            inputStream = new FileInputStream(path);
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
            // reading delay
            if ((line = bufferedReader.readLine()) == null || !line.matches("delay")) {
                Log.error("No scanning interval found in settings file!");
                return -1;
            }
            if ((line = bufferedReader.readLine()) != null) {
                try {
                    scanningIntervalMillis = Integer.parseInt(line);
                    if (scanningIntervalMillis < 1000) {
                        Log.error("Invalid scanning time found in settings file!");
                        return -1;
                    }
                } catch (NumberFormatException e) {
                    Log.error("Invalid scanning time found in settings file!");
                    return -1;
                }
            } else {
                Log.error("No scanning interval found in settings file!");
                return -1;
            }
            // reading folders
            for(;;) {
                if ((line = bufferedReader.readLine()) == null || !line.matches("folder")) {
                    break;
                }
                if ((line = bufferedReader.readLine()) != null) {
                    sourceFolderPaths.add("" + line);
                } else {
                    Log.error("No source folder path found in settings file!");
                    return -1;
                }
                if ((line = bufferedReader.readLine()) != null) {
                    heldFolderPaths.add("" + line);
                } else {
                    Log.error("No held folder path found in settings file!");
                    return -1;
                }
            }
            bufferedReader.close();
        } catch (IOException e) {
            Log.error("Access to settings file failed!");
            return -1;
        }
        return 0;
    }

    private int checkSettings() {
        if (sourceFolderPaths.size() < 1) {
            Log.error("No folders specified in settings file!");
            return -1;
        }
        for (int i = 0; i < sourceFolderPaths.size(); ++i) {
            if (! new File(sourceFolderPaths.get(i)).isDirectory()) {
                Log.error("Invalid source folder found in settings file (" + sourceFolderPaths.get(i) + ")!");
                return -1;
            }
            if (heldFolderPaths.get(i).startsWith(sourceFolderPaths.get(i))
                    || ! new File(sourceFolderPaths.get(i)).isDirectory()) {
                Log.error("Invalid or not existing held folder found in settings file (" + heldFolderPaths + ")!");
                return -1;
            }
        }
        return 0;
    }

    private int scanFolderRecursively(String sourceFolderPath, String heldFolderPath, File currentFolder,
                                      int[] counters, int folderDepth) {
        int maxFolderDepth = 30;
        if (currentFolder == null || ! currentFolder.isDirectory() || folderDepth > maxFolderDepth
                || currentFolder.getAbsoluteFile().toString().startsWith("/proc/")
                || currentFolder.getAbsoluteFile().toString().startsWith("/sys/")) {
            Log.error("Invalid folder detected!");
            return -1;
        }

        File[] thisFolderContent = currentFolder.listFiles();
        if (thisFolderContent == null) {
            Log.error("Folder scanning failed!");
            return -1;
        }
        ++folderDepth;

        for (File thisFolderItem : thisFolderContent) {
            if (thisFolderItem == null) {
                Log.error("Folder scanning failed!");
                return -1;
            }
            if (thisFolderItem.isFile()) {
                processFile(sourceFolderPath, heldFolderPath, thisFolderItem, counters);
                if (Version.DEBUG) {
                    System.out.printf("%-60s  date: %d\n", thisFolderItem.getAbsolutePath(),
                            thisFolderItem.lastModified());
                }
            } else {
                if (thisFolderItem.isDirectory()) {
                    scanFolderRecursively(sourceFolderPath, heldFolderPath, thisFolderItem, counters, folderDepth);
                }
            }
        }
        return 0;
    }

    private int processFile(String sourceFolderPath, String heldFolderPath, File sourceFile, int[] counters) {
        String sourceFilePath = sourceFile.getAbsolutePath();
        String sourceFileRelativePath = sourceFile.getAbsolutePath().substring(sourceFolderPath.length());
        String sourceFileModificationStamp = "." + SystemFunctionality.getDateAndTime(sourceFile.lastModified());
        String sourceFileExtension = sourceFileRelativePath.contains(".")
                ? sourceFileRelativePath.substring(sourceFileRelativePath.lastIndexOf(".")) : "";

        String heldFilePath = heldFolderPath + sourceFileRelativePath + sourceFileModificationStamp
                + sourceFileExtension;
        File heldFileCheck = new File(heldFilePath);
        if (! heldFileCheck.exists()) {
            try {
                File directory = heldFileCheck.getParentFile();
                directory.mkdirs();
                Files.copy(Paths.get(sourceFilePath), Paths.get(heldFilePath),
                        StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                Log.error("Access to source file " + sourceFilePath + " failed!");
            }
            ++counters[NEW_FILES];
        }

        ++counters[SCANNED_FILES];
        return 0;
    }
}
