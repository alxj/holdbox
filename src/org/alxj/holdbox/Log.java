package org.alxj.holdbox;

public class Log {
    public static int ERRORS = 0;

    public static void info(String text) {
        System.out.println("INFO: " + text);
    }

    public static void error(String text) {
        ++ERRORS;
        System.out.println("ERROR: " + text);
    }
}