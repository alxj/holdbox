package org.alxj.holdbox;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class NetworkBeacon extends Thread {
    // "isInitialized = true" allows thread to be started.
    private static boolean isInitialized = false;
    // "isRunning = true" guarantees thread to be running.
    private static boolean isRunning = false;

    private int portNumber = 12050;
    private ServerSocket welcomeSocket;

    public NetworkBeacon(int port) {
        isInitialized = false;
        if (port <= 0 || port >= 65536) {
            Log.error("Invalid port number provided for NetworkBeacon!");
            return;
        }

        portNumber = port;
        isInitialized = true;
    }

    public static boolean isInitialized () {
        return isInitialized;
    }

    public static boolean isRunning () {
        return isRunning;
    }

    public void run() {
        if (!isInitialized) {
            finish();
            return;
        }
        isRunning = true;
        Log.info("Network Beacon is running. It will listen to port:");
        Log.info("    " + portNumber);
        welcomeSocket = null;
        try {
            welcomeSocket = new ServerSocket(portNumber);
        } catch (IOException e) {
            Log.error("Network Beacon failed to establish connection!");
        }
        for(;;) {
            Socket connectionSocket;
            try {
                try {
                    if (! isInitialized) {
                        finish();
                        break;
                    }
                    connectionSocket = welcomeSocket.accept();
                } catch (SocketException e) {
                    finish();
                    break;
                }
                BufferedReader inFromClient
                        = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
                String inputText = inFromClient.readLine();
                String outputText;
                outputText = answer(inputText, connectionSocket.getInetAddress().getHostAddress());
                outToClient.writeBytes(outputText);
            } catch (IOException e) {
                Log.error("Network Beacon failed to handle request!");
            }
        }
    }

    public void interrupt() {
        isInitialized = false;
        if (welcomeSocket != null) {
            try {
                welcomeSocket.close();
            } catch (IOException e) {
                Log.error("Network Beacon failed to close connection!");
            }
        }
    }

    private void finish() {
        isInitialized = isRunning = false;
        Thread.currentThread().interrupt();
        Log.info("Network Beacon is stopped.");
    }

    private String answer(String inputText, String remoteAddress) {
        int maxInputTextLength = 10;
        if (inputText == null || inputText.length() == 0) {
            inputText = "(empty)";
        } else if (inputText.length() > maxInputTextLength) {
            inputText = inputText.substring(0, maxInputTextLength) + "...";
        }
        inputText = inputText.toLowerCase();
        String outputText = "request \"" + inputText
                + "\" from " + remoteAddress
                + " at " + SystemFunctionality.getDateAndTime(System.currentTimeMillis())
                + ". ";
        if (inputText.matches("info")) {
            outputText += "Running: " + HoldboxInstance.isRunning() + ", errors: " + Log.ERRORS + ", uptime: "
                    + SystemFunctionality.getTimeInterval(System.currentTimeMillis() - HoldboxInstance.getStartTime())
                    + ".";
        } else {
            outputText += "Unknown request.";
        }
        Log.info("Answering to network " + outputText);
        return "Holdbox " + Version.get() + " answers to " + outputText + "\n";
    }
}
