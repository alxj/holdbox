package org.alxj.holdbox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        HoldboxAPI holdbox = new HoldboxAPI();
        if (holdbox.requestStart("settings_" + HoldboxAPI.getOperatingSystemStamp() + ".txt") != 0) {
            System.out.println("Terminated.");
            return;
        }

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        String userInput = null;
        for (;;) {
            try {
                userInput = bufferRead.readLine();
            } catch(IOException e) {
                System.out.println("Error: User commands reading failed!");
            }
            if (userInput == null || userInput.toLowerCase().matches("exit")) {
                System.out.println("Got command: exit");
                holdbox.requestStop();
                return;
            } else {
                System.out.println("Unknown command: " + userInput + " (available: exit)");
            }
        }
    }
}
