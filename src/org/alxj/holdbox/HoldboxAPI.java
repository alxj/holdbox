package org.alxj.holdbox;

public class HoldboxAPI {
    private Thread holdboxInstance;
    private Thread networkBeacon;
    private int portNumber = 12050;

    static {
        Log.info("Holdbox " + Version.get());
    }

    public static String getOperatingSystemStamp() {
        return System.getProperty("os.name").toLowerCase().contains("win") ? "win" : "unix";
    }

    public int requestStart(String settingsPath) {
        Log.info("Starting Holdbox.");
        if (isRunning()) {
            Log.error("Holdbox is already running!");
            return -1;
        } else if (! isStopped()) {
            Log.error("Holdbox is switching!");
            return -2;
        }

        networkBeacon = new NetworkBeacon(portNumber);
        if (! NetworkBeacon.isInitialized()) {
            Log.error("Failed to launch Network Beacon!");
            requestStop();
            return -3;
        }
        networkBeacon.start();
        Log.info("Network Beacon is launched.");

        holdboxInstance = new HoldboxInstance(settingsPath);
        if (! HoldboxInstance.isInitialized()) {
            Log.error("Failed to launch Holdbox Instance!");
            requestStop();
            return -4;
        }
        holdboxInstance.start();
        Log.info("Holdbox Instance is launched.");

        return 0;
    }

    public int requestStop() {
        Log.info("Stopping Holdbox.");
        if (holdboxInstance != null) {
            holdboxInstance.interrupt();
            Log.info("Holdbox Instance is requested to finish.");
        } else {
            Log.error("Holdbox Instance is still stopped!");
        }
        if (networkBeacon != null) {
            networkBeacon.interrupt();
            Log.info("Network Beacon is requested to finish.");
        } else {
            Log.error("Network Beacon is still stopped!");
        }
        return 0;
    }

    public boolean isRunning() {
        return (HoldboxInstance.isRunning() && NetworkBeacon.isRunning());
    }

    public boolean isStopped() {
        return (! HoldboxInstance.isRunning() && ! NetworkBeacon.isRunning());
    }

    public int recoverFilesUntilTime(String recoverFolderPath, String timestamp) {
        // todo
        return 0;
    }
}
