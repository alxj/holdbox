package org.alxj.holdbox;

public class Version {
    public final static int VERSION_MAJOR = 0;
    public final static int VERSION_MINOR = 6;

    public final static int YEAR_OF_VERSION = 2013;
    public final static String MONTH_OF_VERSION = "July";

    public final static int BUILD = 46;

    public final static boolean DEBUG = false;

    public static String get() {
        return String.format("%d.%d (%s %d) build %d%s", VERSION_MAJOR, VERSION_MINOR, MONTH_OF_VERSION,
                YEAR_OF_VERSION, BUILD, (DEBUG) ? "d" : "");
    }
}
