package org.alxj.holdbox;

import java.util.Calendar;

public class SystemFunctionality {
    public static String getDateAndTime(long timestamp) {
        Calendar dateAndTime = Calendar.getInstance();
        dateAndTime.setTimeInMillis(timestamp);
        return "[" + formatIntWithZeros(dateAndTime.get(Calendar.YEAR), 4) + "-"
                + formatIntWithZeros(dateAndTime.get(Calendar.MONTH) + 1 /* months start from 0 */, 2)  + "-"
                + formatIntWithZeros(dateAndTime.get(Calendar.DAY_OF_MONTH), 2) + "]["
                + formatIntWithZeros(dateAndTime.get(Calendar.HOUR_OF_DAY), 2) + "."
                + formatIntWithZeros(dateAndTime.get(Calendar.MINUTE), 2) + "."
                + formatIntWithZeros(dateAndTime.get(Calendar.SECOND), 2) + "'"
                + formatIntWithZeros(dateAndTime.get(Calendar.MILLISECOND), 3) + "]";
    }

    public static String getTimeInterval(long milliseconds) {
        int fullSeconds = (int)(milliseconds / 1000);
        int fullMinutes = fullSeconds / 60;
        int fullHours = fullMinutes / 60;
        int fullDays = fullHours / 24;
        String time = formatIntWithZeros(fullSeconds % 60, 2) + "s";
        if (fullMinutes > 0) {
            time = formatIntWithZeros(fullMinutes % 60, 2) + "m " + time;
        }
        if (fullHours > 0) {
            time = formatIntWithZeros(fullHours % 24, 2) + "h " + time;
        }
        if (fullDays > 0) {
            time = fullDays + "d " + time;
        }
        return time;
    }

    private static String formatIntWithZeros(int value, int length) {
        String formatter = "%0" + length + "d";
        return String.format(formatter, value);
    }
}
