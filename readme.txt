Holdbox is a simple Java application for folders backup on desktop platforms. It scans given source folders periodically, and saves their newly changed files to "held" (backup) folders, adding timestamps to file names. This enables source folders recovery at arbitrary time.

Holdbox reads parameters from settings_unix.txt (for Unix-type operating systems) or settings_win.txt (for Windows operating systems).

	Structure of settings file (number of folders can be arbitrary):
delay
<update interval in milliseconds>
folder
<path to source folder 1>
<path to existing held folder 1>
folder
<path to source folder 2>
<path to existing held folder 2>

	Example of settings_unix.txt:
delay
60000
folder
/home/John/Documents
/home/John/DocumentsBackup
folder
/home/John/Photos
/home/John/PhotosArchive

	Example of settings_win.txt:
delay
60000
folder
C:\Documents
C:\DocumentsBackup
folder
C:\Photos
C:\PhotosArchive

	The application can be launched like:
java -jar Holdbox.jar

	To finish the application, enter
exit
